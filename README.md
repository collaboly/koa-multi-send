# koa-multi-send

Like koa-send, optionally support multiple root(s) 

## configuration options 

Same as [koa-send]() except pass `roots` instead of `root`.
This is not meant to use as a standalone koa middleware. 
It's been develop with [koa-hijack]() a static serve with additional features.

---

If you use this module, please do give it a star, thanks.

---

MIT 

Brought to you by [newbran.ch](https://newbran.ch) & [Joel Chu](https://joelchu.com)