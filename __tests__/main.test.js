/**
 * Main test
 */
process.env.DEBUG='koa-multi-send*';
const test = require('ava');
const request = require('superkoa');
const send = require('../index');
const Koa = require('koa');
const path = require('path');


test.skip('It should be 404', async (t) => {
  try {
    const app = new Koa();
    app.use(async (ctx) => {
        await send(ctx, path.join(__dirname, '/fixtures/hello.txt'));
    });
    const res = await request(app)
        .get('/')
    t.is(404, res.status);
  } catch (e) {
    console.log('func error 1', e);
  }
});

test('It should able to serve up multiple folders', async (t) => {
  try {
    const app = new Koa();
    app.use(async (ctx) => {
      await send(ctx, 'index.html', {root: path.join(__dirname, 'fixtures', 'base')});
    });
    const res = await request(app)
      .get('/')
    t.is(200, res.status);
  } catch (e) {
    console.log('func error 2', e);
  }
});
