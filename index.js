/**
 * Module dependencies.
 */

const debug = require('debug')('koa-multi-send:main')
const resolvePath = require('resolve-path')
const createError = require('http-errors')
const assert = require('assert')
const fs = require('mz/fs')
// const fs = require('fs');
const {
  normalize,
  basename,
  extname,
  resolve,
  parse,
  sep
} = require('path')
let lastErr;
let lastErrStatus;
/**
 * Expose `send()`.
 */
// debug('wtf', fs);
module.exports = multiSend;

/**
 * Send file at `path` with the
 * given `options` to the koa `ctx`.
 *
 * @param {Context} ctx
 * @param {String} path
 * @param {Object} [opts]
 * @return {Function}
 * @api public
 */
async function multiSend (ctx, path, opts = {}) {
  assert(ctx, 'koa context required');
  assert(path, 'pathname required');
  // options
  debug('send "%s" %j', path, opts);
  // The root is optional so we need to double check if that is presented or not
  const roots = opts.root ? Array.isArray(opts.root) ? opts.root : [opts.root] : [''];
  const trailingSlash = path[path.length - 1] === '/'
  const index = opts.index
  const maxage = opts.maxage || opts.maxAge || 0
  const immutable = opts.immutable || false
  const hidden = opts.hidden || false
  const format = opts.format !== false
  const extensions = Array.isArray(opts.extensions) ? opts.extensions : false
  const brotli = opts.brotli !== false
  const gzip = opts.gzip !== false
  const setHeaders = opts.setHeaders

  if (setHeaders && typeof setHeaders !== 'function') {
    throw new TypeError('option setHeaders must be function')
  }

  // From this point onward need to change to accept array of root(s)
  const result = await getFileStats(ctx, {
    roots,
    path,
    hidden,
    brotli,
    gzip,
    extensions,
    format,
    index,
    trailingSlash
  });
  debug('result', result);
  // that means the file is not found
  if (result) {
    // Success and found the file - serve it up
    const {
      rootpath,
      stats,
      encodingExt
    } = result[0];

    if (encodingExt!=='') {
      ctx.set('Content-Encoding', encodingExt==='gz' ? 'gzip' : encodingExt);
      ctx.res.removeHeader('Content-Length');
    }
    if (setHeaders) {
      setHeaders(ctx.res, rootpath, stats)
    }
    // stream
    ctx.set('Content-Length', stats.size)
    if (!ctx.response.get('Last-Modified')) {
      ctx.set('Last-Modified', stats.mtime.toUTCString())
    }
    if (!ctx.response.get('Cache-Control')) {
      const directives = ['max-age=' + (maxage / 1000 | 0)]
      if (immutable) {
        directives.push('immutable')
      }
      ctx.set('Cache-Control', directives.join(','))
    }
    if (!ctx.type) {
      ctx.type = type(rootpath, encodingExt)
    }
    ctx.body = fs.createReadStream(rootpath);
  } else {
    debug('throw at the end', lastErrStatus, lastErr);
    // just throw error back
    throw createError(lastErrStatus || 404, lastErr);
  }
}

/**
 * Extra wrapper just for getting the stats
 * @param {object} ctx koa
 * @param {object} params options
 * @return {object} result or throw
 */
async function getFileStats(ctx, params) {
  const result = await getRootpath(ctx, params);
  return result.map(async (res) => {
    if (typeof res === 'object') {
      debug('result', res);
      const {rootpath, encodingExt} = res;
      let stats;
      try {
        stats = await fs.statSync(rootpath);
        // debug('stats', stats);
        // Format the path to serve static file servers
        // and not require a trailing slash for directories,
        // so that you can do both `/directory` and `/directory/`
        if (stats.isDirectory()) {
          if (format && index) {
            rootpath += '/' + index
            stats = await fs.statSync(rootpath);
          } else {
            debug('isDirectory???');
            return;
          }
        }
        debug('result', rootpath);
        return {rootpath, stats, encodingExt};
      } catch (err) {
        const notfound = ['ENOENT', 'ENAMETOOLONG', 'ENOTDIR']
        if (notfound.includes(err.code)) {
          lastErrStatus = 404;
          lastErr = err;
          // throw createError(404, err)
          return false;
        }
        debug('err.status 500', err);
        // err.status = 500
        // throw err
        lastErrStatus = 500;
        lastErr = err;
        return false;
      }
    }
    return false;
  }).reduce((last, next) => {
    if (!last && !next) {
      return false;
    }
    return next;
  }, false);
}


/**
 * Break out the above loop method to filter out the unfound path
 *
 * @param {object} ctx koa context
 * @param {object} params all the options
 * @return {array} filtered array of the path and stats
 */
async function getRootpath(ctx, params) {
  const {
    roots,
    path,
    hidden,
    brotli,
    gzip,
    extensions,
    format,
    index,
    trailingSlash
  } = params;
  // loop start
  return roots.filter(async (root) => {
    // normalize path
    let _path = decode(path.substr(parse(path).root.length));
    // path = decode(path)
    if (_path === -1) {
      lastErrStatus = 404;
      lastErr = 'failed to decode';
      debug('path error', lastErr);
      return false;
      // return ctx.throw(400, 'failed to decode')
    }
    // index file support
    if (index && trailingSlash) {
      _path += index;
    }
    let encodingExt = '';
    let rootpath = resolvePath(root, _path);
    debug('rootpath', rootpath);
    // hidden file support, ignore
    if (!hidden && isHidden(root, rootpath)) {
      lastErr = 'is hidden path';
      lastErrStatus = 500;
      debug('isHidden error', lastErr);
      return false;
    }
    // serve brotli file when possible otherwise gzipped file when possible
    if (ctx.acceptsEncodings('br', 'identity') === 'br' && brotli && (await fs.exists(rootpath + '.br'))) {
      encodingExt = 'br';
    } else if (ctx.acceptsEncodings('gzip', 'identity') === 'gzip' && gzip && (await fs.exists(rootpath + '.gz'))) {
      encodingExt = 'gz';
    }
    if (encodingExt !== '') {
      rootpath = rootpath + '.' + encodingExt;
    }
    if (extensions && !/\.[^/]*$/.exec(rootpath)) {
      const list = [].concat(extensions)
      for (let i = 0; i < list.length; i++) {
        let ext = list[i]
        if (typeof ext !== 'string') {
          throw new TypeError('option extensions must be array of strings or false')
        }
        if (!/^\./.exec(ext)) {
          ext = '.' + ext;
        }
        if (await fs.exists(rootpath + ext)) {
            rootpath = rootpath + ext;
          break;
        }
      }
    }
    return {rootpath, encodingExt};
  });
};

/**
 * normalize resolve
 * @param {string} path directories
 * @return {string} resolved normalize path
 */
function normalizeResolve(path) {
  return normalize(resolve(path));
}

/**
 * Check if it's hidden.
 */

function isHidden (root, path) {
  path = path.substr(root.length).split(sep)
  for (let i = 0; i < path.length; i++) {
    if (path[i][0] === '.') return true
  }
  return false
}

/**
 * File type.
 */

function type (file, ext) {
  return ext !== '' ? extname(basename(file, ext)) : extname(file)
}

/**
 * Decode `path`.
 */

function decode (path) {
  try {
    return decodeURIComponent(path)
  } catch (err) {
    return -1
  }
}
